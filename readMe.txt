Tyger tyger, burning bright, in the forests of the night.
What immortal hand or eye shall fear thy fearful symmetry.
